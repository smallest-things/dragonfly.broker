package handlers;

import data.Store;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.mqtt.MqttEndpoint;
import io.vertx.mqtt.messages.MqttPublishMessage;

public class PublishHandler {
  public static Handler<MqttPublishMessage> handler(MqttEndpoint mqttEndpoint) {
    return mqttPublishMessage -> {
      var message = mqttPublishMessage.payload().toString(java.nio.charset.Charset.defaultCharset());

      System.out.println("From: " + mqttEndpoint.clientIdentifier());
      System.out.println("New message on topic: " + mqttPublishMessage.topicName() + " : " + message);

      /* --- dispatch messages to all subscribed clients --- */
      Store.getMqttSubscriptions().forEach((id, mqttSubscription) -> {
        if(mqttSubscription.getTopic().equals(mqttPublishMessage.topicName()) && mqttSubscription.getMqttEndpoint().isConnected()) {
          mqttSubscription.getMqttEndpoint().publish(mqttPublishMessage.topicName(), Buffer.buffer(message),mqttPublishMessage.qosLevel(),false, false);
        }
      });


      switch (mqttPublishMessage.qosLevel()) {
        case AT_LEAST_ONCE:
          mqttEndpoint.publishAcknowledge(mqttPublishMessage.messageId());
          break;
        case EXACTLY_ONCE:
          mqttEndpoint.publishReceived(mqttPublishMessage.messageId());
          break;
        case FAILURE:
          // TODO
          break;
      }
    };
  }
}
