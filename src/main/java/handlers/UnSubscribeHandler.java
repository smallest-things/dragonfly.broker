package handlers;

import io.vertx.core.Handler;
import io.vertx.mqtt.MqttEndpoint;
import io.vertx.mqtt.messages.MqttUnsubscribeMessage;

public class UnSubscribeHandler {

  public static Handler<MqttUnsubscribeMessage> handler(MqttEndpoint mqttEndpoint) {
    return mqttUnsubscribeMessage -> {
      System.out.println("👋 bye");
    };
  }

}
