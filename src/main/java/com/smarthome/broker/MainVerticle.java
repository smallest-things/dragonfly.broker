package com.smarthome.broker;

import handlers.EndPointHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.mqtt.MqttServer;
import io.vertx.mqtt.MqttServerOptions;

import java.util.Optional;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void stop(Promise<Void> stopPromise) throws Exception {
    System.out.println("Broker is stopped");
    stopPromise.complete();
  }

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    /*
      Define parameters of the application
      ------------------------------------
     */

    // +++ MQTT +++
    var mqttPort = Integer.parseInt(Optional.ofNullable(System.getenv("MQTT_PORT")).orElse("1883"));
    var mqttOptions = new MqttServerOptions().setPort(mqttPort);
    var mqttServer = MqttServer.create(vertx, mqttOptions);

    /*
      Create handlers for the broker
      ------------------------------------
     */
    mqttServer.endpointHandler(EndPointHandler.handler);
    /*
      Create and start the MQTT broker
      ------------------------------------
     */
    mqttServer.listen()
      .onFailure(error -> {
        System.out.println("MQTT " + error.getMessage());
      })
      .onSuccess(ok -> {
        System.out.println("MQTT broker started, listening on " + mqttPort);
      });
  }
}
